#ifdef OPENAL_WRAPPER_AUDITORY_TESTS
#include <catch2/catch.hpp>
#include <thread>
#include <openal-wrapper/initializer.hh>
#include <openal-converter/audio_file.hh>
#include <openal-wrapper/buffer.hh>
#include <openal-wrapper/source.hh>
#include <openal-wrapper/listener.hh>

SCENARIO("openal-wrapper can be used to easily play sounds with OpenAL", "[openal]")
{
    GIVEN("An initializes OpenAL context")
    {
        oal_wrapper::initializer_type initializer;

        AND_GIVEN("A buffer_type object")
        {
            oal_converter::audio_file_type audio_file("./sounds/test-mono.flac");
            const oal_converter::audio_buffer_type audio_buffer = audio_file.get_buffer(oal_converter::channel_layout_type::mono,
                                                                                        oal_converter::format_type::s16);
            const oal_wrapper::buffer_type buffer;

            buffer.set_data(audio_buffer);

            AND_GIVEN("A source_type object connected to the buffer_type object")
            {
                const oal_wrapper::source_type source;

                source.set_buffer(buffer);
                source.set_position(-1.0f, 0.0f, 0.0f);
                source.set_direction(1.0f, 0.0f, 0.0f);

                AND_GIVEN("A listener set up in the center facing forward")
                {
                    oal_wrapper::listener_type::set_position(0.0f, 0.0f, 0.0f);
                    oal_wrapper::listener_type::set_orientation(0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f);

                    source.play();
                    while(source.is_playing())
                    {
                        std::this_thread::sleep_for(std::chrono::milliseconds(10));
                    }
                }
            }
        }
    }
}
#endif