#include <catch2/catch.hpp>
#include <openal-wrapper/device.hh>
#include <openal-wrapper/context.hh>

SCENARIO("context_tpe objects can be created with a device_type object", "[context]")
{
    GIVEN("A default constructed device_type object")
    {
        const oal_wrapper::device_type device;

        REQUIRE(device.get_device() != nullptr);
        AND_GIVEN("A context_type object")
        {
            const oal_wrapper::context_type context(device);

            THEN("The context_type object can be used to set the current context")
            {
                REQUIRE_NOTHROW(context.make_current());
            }
        }
    }
}
