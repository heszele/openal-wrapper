#include <catch2/catch.hpp>
#include <openal-wrapper/initializer.hh>
#include <openal-wrapper/buffer.hh>
#include <openal-wrapper/exception.hh>

SCENARIO("initializer_type objects can be used to easily initialize OpenAL device and context", "[initializer]")
{
    WHEN("No initializer_type objects are created")
    {
        THEN("buffer_type objects cannot be created either, since there is no initialized context")
        {
            REQUIRE_THROWS_AS(oal_wrapper::buffer_type(), oal_wrapper::buffer_type::exception_type);

            AND_WHEN("An initialzer_type object is created")
            {
                oal_wrapper::initializer_type* initializer = new oal_wrapper::initializer_type();

                THEN("buffer_type objects can be created, since the context is initialized")
                {
                    REQUIRE_NOTHROW(oal_wrapper::buffer_type());

                    AND_WHEN("The initializer_type object is being destroyed")
                    {
                        REQUIRE_NOTHROW(delete initializer);

                        THEN("buffer_type objects are again cannot be created")
                        {
                            REQUIRE_THROWS_AS(oal_wrapper::buffer_type(), oal_wrapper::buffer_type::exception_type);
                        }
                    }
                }
            }
        }
    }
}
