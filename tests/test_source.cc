#include <catch2/catch.hpp>
#include <openal-wrapper/initializer.hh>
#include <openal-wrapper/source.hh>
#include <openal-wrapper/buffer.hh>
#include <openal-converter//audio_file.hh>
#include <AL/al.h>

SCENARIO("source_type can be used to set values on OpenAL sources", "[source]")
{
    GIVEN("An initialized OpanAL context")
    {
        oal_wrapper::initializer_type initializer;

        THEN("source_type can be used to set values")
        {
            const oal_wrapper::source_type source;

            source.set_position(1.0f, 2.0f, 3.0f);
            source.set_direction(4.0f, 5.0f, 6.0f);
            source.set_velocity(7.0f, 8.0f, 9.0f);
            source.set_gain(0.1f);
            source.set_pitch(0.5f);

            float x = 0.0f;
            float y = 0.0f;
            float z = 0.0f;

            alGetSource3f(source.get_source(), AL_POSITION, &x, &y, &z);
            CHECK(x == 1.0f);
            CHECK(y == 2.0f);
            CHECK(z == 3.0f);

            alGetSource3f(source.get_source(), AL_DIRECTION, &x, &y, &z);
            CHECK(x == 4.0f);
            CHECK(y == 5.0f);
            CHECK(z == 6.0f);

            alGetSource3f(source.get_source(), AL_VELOCITY, &x, &y, &z);
            CHECK(x == 7.0f);
            CHECK(y == 8.0f);
            CHECK(z == 9.0f);

            float gain = 0.0f;

            alGetSourcef(source.get_source(), AL_GAIN, &gain);
            CHECK(gain == 0.1f);

            float pitch = 0.0f;

            alGetSourcef(source.get_source(), AL_PITCH, &pitch);
            CHECK(pitch == 0.5f);
        }
    }
}

SCENARIO("source_type object will not start playing without an attached buffer")
{
     GIVEN("An initialized OpanAL context")
    {
        oal_wrapper::initializer_type initializer;

        AND_GIVEN("A source_type object")
        {
            oal_wrapper::source_type source;

            CHECK_FALSE(source.is_playing());
            CHECK_FALSE(source.is_paused());
            CHECK(source.is_stopped());
            THEN("The source type object will not start playing anything without an attached buffer")
            {
                source.play();
                CHECK_FALSE(source.is_playing());
                CHECK_FALSE(source.is_paused());
                CHECK(source.is_stopped());

                source.pause();
                CHECK_FALSE(source.is_playing());
                CHECK_FALSE(source.is_paused());
                CHECK(source.is_stopped());

                source.stop();
                CHECK_FALSE(source.is_playing());
                CHECK_FALSE(source.is_paused());
                CHECK(source.is_stopped());
            }
        }
     }
}

SCENARIO("source_type can be used to play sounds", "[source]")
{
    GIVEN("An initialized OpanAL context")
    {
        oal_wrapper::initializer_type initializer;

        AND_GIVEN("A buffer_type object")
        {
            oal_converter::audio_file_type audio_file("./sounds/test-mono.flac");
            const oal_converter::audio_buffer_type audio_buffer = audio_file.get_buffer(oal_converter::channel_layout_type::mono,
                                                                                        oal_converter::format_type::s16);
            const oal_wrapper::buffer_type buffer;

            buffer.set_data(audio_buffer);

            THEN("source_type can be used to attach it to the buffer and play the sound")
            {
                const oal_wrapper::source_type source;

                source.set_buffer(buffer);
                CHECK_FALSE(source.is_playing());
                CHECK_FALSE(source.is_paused());
                CHECK(source.is_stopped());

                source.play();
                CHECK(source.is_playing());
                CHECK_FALSE(source.is_paused());
                CHECK_FALSE(source.is_stopped());

                source.pause();
                CHECK_FALSE(source.is_playing());
                CHECK(source.is_paused());
                CHECK_FALSE(source.is_stopped());

                source.stop();
                CHECK_FALSE(source.is_playing());
                CHECK_FALSE(source.is_paused());
                CHECK(source.is_stopped());

                AND_THEN("source_type's buffer can be reset and then no sound will be played")
                {
                    source.reset_buffer();
                    CHECK_FALSE(source.is_playing());
                    CHECK_FALSE(source.is_paused());
                    CHECK(source.is_stopped());

                    source.play();
                    CHECK_FALSE(source.is_playing());
                    CHECK_FALSE(source.is_paused());
                    CHECK(source.is_stopped());

                    source.pause();
                    CHECK_FALSE(source.is_playing());
                    CHECK_FALSE(source.is_paused());
                    CHECK(source.is_stopped());

                    source.stop();
                    CHECK_FALSE(source.is_playing());
                    CHECK_FALSE(source.is_paused());
                    CHECK(source.is_stopped());
                }
            }
        }
    }
}
