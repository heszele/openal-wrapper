#include <catch2/catch.hpp>
#include <openal-wrapper/initializer.hh>
#include <openal-wrapper/listener.hh>

SCENARIO("listener_type can be used to set values on the OpenAL listener", "[listener]")
{
    GIVEN("An initialized OpanAL context")
    {
        oal_wrapper::initializer_type initializer;

        THEN("listener_type can be used to set values")
        {
            oal_wrapper::listener_type::set_velocity(1.0f, 2.0f, 3.0f);
            oal_wrapper::listener_type::set_position(4.0f, 5.0f, 6.0f);
            oal_wrapper::listener_type::set_orientation(7.0f, 8.0f, 9.0f,
                                                        10.0f, 11.0f, 12.0f);
            oal_wrapper::listener_type::set_gain(0.1f);

            float x = 0.0f;
            float y = 0.0f;
            float z = 0.0f;

            alGetListener3f(AL_VELOCITY, &x, &y, &z);
            CHECK(x == 1.0f);
            CHECK(y == 2.0f);
            CHECK(z == 3.0f);

            alGetListener3f(AL_POSITION, &x, &y, &z);
            CHECK(x == 4.0f);
            CHECK(y == 5.0f);
            CHECK(z == 6.0f);

            float orientation[6] = { 0.0f };

            alGetListenerfv(AL_ORIENTATION, orientation);
            CHECK(orientation[0] == 7.0f);
            CHECK(orientation[1] == 8.0f);
            CHECK(orientation[2] == 9.0f);
            CHECK(orientation[3] == 10.0f);
            CHECK(orientation[4] == 11.0f);
            CHECK(orientation[5] == 12.0f);

            float gain = 0.0f;

            alGetListenerf(AL_GAIN, &gain);
            CHECK(gain == 0.1f);
        }
    }
}
