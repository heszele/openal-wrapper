#include <catch2/catch.hpp>
#include <openal-wrapper/device.hh>

SCENARIO("device_type objects can be created", "[device]")
{
    WHEN("A device_type object is default constructed it will not throw")
    {
        const oal_wrapper::device_type* device = nullptr;

        REQUIRE_NOTHROW(device = new oal_wrapper::device_type());
        REQUIRE(nullptr != device);
        REQUIRE(device->get_device() != nullptr);

        AND_THEN("It can be used for errors")
        {
            REQUIRE_NOTHROW(device->check_errors());
        }

        REQUIRE_NOTHROW(delete device);
    }
}
