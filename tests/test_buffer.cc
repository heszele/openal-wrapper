#include <catch2/catch.hpp>
#include <openal-wrapper/buffer.hh>
#include <openal-wrapper/device.hh>
#include <openal-wrapper/context.hh>
#include <openal-converter/audio_file.hh>

SCENARIO("buffer_type objects cannot be created without a context", "[buffer]")
{
    WHEN("A buffer_type object is created without context, it throws an error")
    {
        REQUIRE_THROWS_AS(oal_wrapper::buffer_type(), oal_wrapper::buffer_type::exception_type);
    }
}

SCENARIO("buffer_type objects can be created when a context is created", "[buffer]")
{
    GIVEN("An OpenAL device and a current context")
    {
        const oal_wrapper::device_type device;
        const oal_wrapper::context_type context(device);

        REQUIRE_NOTHROW(context.make_current());
        THEN("A buffer_type object can be constructed")
        {
            const oal_wrapper::buffer_type buffer;

            REQUIRE(buffer.get_buffer() != 0);

            AND_THEN("Data can be set on the buffer_type object")
            {
                const oal_converter::audio_buffer_type audio_buffer =  oal_converter::audio_file_type::get_buffer("./sounds/test-mono.flac",
                                                                                                                  oal_converter::channel_layout_type::mono,
                                                                                                                  oal_converter::format_type::s16);
                buffer.set_data(audio_buffer);
            }
        }
    }
}
