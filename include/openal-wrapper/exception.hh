#pragma once
#include <exception>
#include <fmt/format.h>
#include <AL/al.h>

namespace oal_wrapper
{
    class exception_type: public std::exception
    {
    public:
        template<typename concrete_type = exception_type>
        static void check_errors()
        {
            const ALenum error = alGetError();

            static_assert(std::is_base_of<exception_type, concrete_type>::value, "Only descendants of exception_type can be used!");
            if(AL_NO_ERROR != error)
            {
                throw concrete_type("Got OpenAL error: {}", error);
            }
        }

        static void clear_errors();

        template<typename... argument_types>
        exception_type(const fmt::string_view& format, argument_types&&... arguments):
            _message(fmt::format(format, std::forward<argument_types>(arguments)...))
        {
            // Nothing to do yet
        }

    private:
        virtual char const* what() const noexcept override;

        std::string _message;
    };
}
