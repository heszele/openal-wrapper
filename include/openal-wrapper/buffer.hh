#pragma once
#include "exception.hh"
#include <AL/al.h>
#include <openal-converter/audio_buffer.hh>

namespace oal_wrapper
{
    class buffer_type
    {
    public:
        class exception_type: public oal_wrapper::exception_type
        {
        public:
            using oal_wrapper::exception_type::exception_type;
        };

        using audio_buffer_type =  oal_converter::audio_buffer_type;

        buffer_type();
        buffer_type(const buffer_type&) = delete;
        buffer_type(buffer_type&&) = delete;
        ~buffer_type();

        buffer_type& operator=(const buffer_type&) = delete;
        buffer_type& operator=(buffer_type&&) = delete;

        ALuint get_buffer() const;

        void set_data(const audio_buffer_type& buffer) const;

    private:
        static ALenum get_openal_format(const oal_converter::audio_buffer_type& buffer);

        ALuint _buffer = 0;
    };
}
