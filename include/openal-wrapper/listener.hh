#pragma once
#include "exception.hh"

namespace oal_wrapper
{
    class listener_type
    {
    public:
        class exception_type: public oal_wrapper::exception_type
        {
        public:
            using oal_wrapper::exception_type::exception_type;
        };

        listener_type() = delete;
        listener_type(const listener_type&) = delete;
        listener_type(listener_type&&) = delete;

        ~listener_type() = delete;

        listener_type& operator=(const listener_type&) = delete;
        listener_type& operator=(listener_type&&) = delete;

        static void set_gain(float gain);
        static void set_position(float x, float y, float z);
        static void set_velocity(float x, float y, float z);
        static void set_orientation(float at_x, float at_y, float at_z,
                                    float up_x, float up_y, float up_z);
    };
}
