#pragma once
#include "exception.hh"

struct ALCcontext;

namespace oal_wrapper
{
    class device_type;

    class context_type
    {
    public:
        class exception_type: public oal_wrapper::exception_type
        {
        public:
            using oal_wrapper::exception_type::exception_type;
        };

        context_type(const device_type& device);
        context_type(const context_type&) = delete;
        context_type(context_type&&) = delete;

        ~context_type();

        context_type& operator=(const context_type&) = delete;
        context_type& operator=(context_type&&) = delete;

        void make_current() const;
    
    private:
        ALCcontext* _context = nullptr;
    };
}
