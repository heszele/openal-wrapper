#pragma once
#include "device.hh"
#include "context.hh"

namespace oal_wrapper
{
    class initializer_type
    {
    public:
        initializer_type();
        initializer_type(const initializer_type&) = delete;
        initializer_type(initializer_type&&) = delete;

        ~initializer_type() = default;

        initializer_type& operator=(const initializer_type&) = delete;
        initializer_type& operator=(initializer_type&&) = delete;

    private:
        device_type _device;
        context_type _context;
    };
}
