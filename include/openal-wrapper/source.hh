#pragma once
#include <AL/al.h>
#include "exception.hh"

namespace oal_wrapper
{
    class buffer_type;

    class source_type
    {
    public:
        class exception_type: public oal_wrapper::exception_type
        {
        public:
            using oal_wrapper::exception_type::exception_type;
        };

        source_type();
        source_type(const source_type&) = delete;
        source_type(source_type&&) = delete;

        ~source_type();

        source_type& operator=(const source_type&) = delete;
        source_type& operator=(source_type&&) = delete;

        ALuint get_source() const;

        void set_buffer(const buffer_type& buffer) const;
        void reset_buffer() const;

        void set_gain(float gain) const;
        void set_pitch(float pitch) const;
        void set_position(float x, float y, float z) const;
        void set_direction(float x, float y, float z) const;
        void set_velocity(float x, float y, float z) const;

        void play() const;
        void pause() const;
        void stop() const;

        bool is_playing() const;
        bool is_paused() const;
        bool is_stopped() const;

    private:
        ALint get_state() const;

        ALuint _source = 0;
    };
}
