#pragma once
#include "exception.hh"

struct ALCdevice;

namespace oal_wrapper
{
    class device_type
    {
    public:
        class exception_type: public oal_wrapper::exception_type
        {
        public:
            using oal_wrapper::exception_type::exception_type;
        };

        device_type();
        device_type(const device_type&) = delete;
        device_type(device_type&&) = delete;

        ~device_type();

        device_type& operator=(const device_type&) = delete;
        device_type& operator=(device_type&&) = delete;

        ALCdevice* get_device() const;
        void check_errors() const;
    
    private:
        ALCdevice* _device = nullptr;
    };
}
