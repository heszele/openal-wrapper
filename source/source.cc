#include <openal-wrapper/source.hh>
#include <openal-wrapper/buffer.hh>

namespace oal_wrapper
{
    source_type::source_type()
    {
        alGenSources(1, &_source);
        exception_type::check_errors();
        if(0 == _source)
        {
            throw exception_type("Failed to generate OpenAL source!");
        }
        // Default values
        set_gain(1.0f);
        set_pitch(1.0f);
        set_position(0.0f, 0.0f, 0.0f);
        set_direction(1.0f, 0.0f, 0.0f);
    }

    source_type::~source_type()
    {
        alDeleteSources(1, &_source);
        exception_type::check_errors<exception_type>();
        _source = 0;
    }

    ALuint source_type::get_source() const
    {
        return _source;
    }

    void source_type::set_buffer(const buffer_type& buffer) const
    {
        alSourcei(_source, AL_BUFFER, buffer.get_buffer());
        exception_type::check_errors<exception_type>();
    }

    void source_type::reset_buffer() const
    {
        alSourcei(_source, AL_BUFFER, 0);
        exception_type::check_errors<exception_type>();
    }

    void source_type::set_gain(float gain) const
    {
        alSourcef(_source, AL_GAIN, gain);
        exception_type::check_errors<exception_type>();
    }

    void source_type::set_pitch(float pitch) const
    {
        alSourcef(_source, AL_PITCH, pitch);
        exception_type::check_errors<exception_type>();
    }

    void source_type::set_position(float x, float y, float z) const
    {
        alSource3f(_source, AL_POSITION, x, y, z);
        exception_type::check_errors<exception_type>();
    }

    void source_type::set_direction(float x, float y, float z) const
    {
        alSource3f(_source, AL_DIRECTION, x, y, z);
        exception_type::check_errors<exception_type>();
    }

    void source_type::set_velocity(float x, float y, float z) const
    {
        alSource3f(_source, AL_VELOCITY, x, y, z);
        exception_type::check_errors<exception_type>();
    }

    void source_type::play() const
    {
        alSourcePlay(_source);
        exception_type::check_errors<exception_type>();
    }

    void source_type::pause() const
    {
        alSourcePause(_source);
        exception_type::check_errors<exception_type>();
    }

    void source_type::stop() const
    {
        alSourceStop(_source);
        exception_type::check_errors<exception_type>();
    }

    bool source_type::is_playing() const
    {
        return AL_PLAYING == get_state();
    }

    bool source_type::is_paused() const
    {
        return AL_PAUSED == get_state();
    }

    bool source_type::is_stopped() const
    {
        const ALint state = get_state();

        // Initial state is also considered as stopped
        return AL_STOPPED == state || AL_INITIAL == state;
    }

    ALint source_type::get_state() const
    {
        ALint state = 0;
        alGetSourcei(_source, AL_SOURCE_STATE, &state);
        exception_type::check_errors<exception_type>();

        return state;
    }
}
