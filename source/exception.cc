#include <openal-wrapper/exception.hh>
#include <AL/al.h>
#include <string>

namespace oal_wrapper
{
    void exception_type::clear_errors()
    {
        ALenum error = alGetError();

        while(AL_NO_ERROR != error)
        {
            error = alGetError();
        }
    }

    char const* exception_type::what() const noexcept
    {
        return _message.c_str();
    }
}
