#include <openal-wrapper/initializer.hh>

namespace oal_wrapper
{
    initializer_type::initializer_type():
        _context(_device)
    {
        _context.make_current();
    }
}
