#include <openal-wrapper/buffer.hh>

namespace oal_wrapper
{
    buffer_type::buffer_type()
    {
        alGenBuffers(1, &_buffer);
        exception_type::check_errors<exception_type>();
        if(0 == _buffer)
        {
            throw exception_type("Failed to generate OpenAL buffer!");
        }
    }

    buffer_type::~buffer_type()
    {
        alDeleteBuffers(1, &_buffer);
        exception_type::check_errors<exception_type>();
        _buffer = 0;
    }

    ALuint buffer_type::get_buffer() const
    {
        return _buffer;
    }

    void buffer_type::set_data(const audio_buffer_type& buffer) const
    {
        alBufferData(_buffer,
                     get_openal_format(buffer),
                     buffer.get_buffer().data(),
                     ALsizei(buffer.get_buffer().size()),
                     buffer.get_frequency());
        exception_type::check_errors<exception_type>();
    }

    ALenum buffer_type::get_openal_format(const oal_converter::audio_buffer_type& buffer)
    {
        switch (buffer.get_channel_layout())
        {
        case oal_converter::channel_layout_type::stereo:
            switch(buffer.get_format())
            {
            case oal_converter::format_type::u8:
                return AL_FORMAT_STEREO8;
            case oal_converter::format_type::s16:
                return AL_FORMAT_STEREO16;
            default:
                throw exception_type("Invalid audio buffer format: {}!",
                                     buffer.get_format());
            }
        case oal_converter::channel_layout_type::mono:
            switch(buffer.get_format())
            {
            case oal_converter::format_type::u8:
                return AL_FORMAT_MONO8;
            case oal_converter::format_type::s16:
                return AL_FORMAT_MONO16;
            default:
                throw exception_type("Invalid audio buffer format: {}!",
                                     buffer.get_format());
            }
        default:
            throw exception_type("Invalid audio buffer channel layout: {}!",
                                 buffer.get_channel_layout());
        }
    }
}
