#include <openal-wrapper/context.hh>
#include <AL/alc.h>
#include <openal-wrapper/device.hh>

namespace oal_wrapper
{
    context_type::context_type(const device_type& device):
        _context(alcCreateContext(device.get_device(), nullptr))
    {
        if(nullptr == _context)
        {
            throw exception_type("Failed to create OpenAL context!");
        }
        device.check_errors();
    }

    context_type::~context_type()
    {
        alcDestroyContext(_context);
        _context = nullptr;
    }

    void context_type::make_current() const
    {
        if(ALC_FALSE == alcMakeContextCurrent(_context))
        {
            throw exception_type("Failed to make current context!");
        }
    }
}
