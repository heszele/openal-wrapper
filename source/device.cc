#include <openal-wrapper/device.hh>
#include <AL/alc.h>

namespace oal_wrapper
{
    device_type::device_type():
        _device(alcOpenDevice(nullptr))
    {
        if(nullptr == _device)
        {
            throw exception_type("Failed to create OpenAL device!");
        }
        check_errors();
    }

    device_type::~device_type()
    {
        alcCloseDevice(_device);
        _device = nullptr;
    }

    ALCdevice* device_type::get_device() const
    {
        return _device;
    }

    void device_type::check_errors() const
    {
        const ALCenum error = alcGetError(_device);

        if(ALC_NO_ERROR != error)
        {
            throw exception_type("Got OpenAL error: {}", error);
        }
    }
}
