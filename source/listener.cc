#include <openal-wrapper/listener.hh>
#include <AL/al.h>

namespace oal_wrapper
{
    void listener_type::set_gain(float gain)
    {
        alListenerf(AL_GAIN, gain);
        exception_type::check_errors<exception_type>();
    }

    void listener_type::set_position(float x, float y, float z)
    {
        alListener3f(AL_POSITION, x, y, z);
        exception_type::check_errors<exception_type>();
    }

    void listener_type::set_velocity(float x, float y, float z)
    {
        alListener3f(AL_VELOCITY, x, y, z);
        exception_type::check_errors<exception_type>();
    }

    void listener_type::set_orientation(float at_x, float at_y, float at_z,
                                        float up_x, float up_y, float up_z)
    {
        float orientation[] = {
            at_x, at_y, at_z,
            up_x, up_y, up_z,
        };

        alListenerfv(AL_ORIENTATION, orientation);
        exception_type::check_errors<exception_type>();
    }
}
